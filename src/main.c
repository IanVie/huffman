#include <stdio.h>
#include <stdlib.h>
#include "heap.h"
#include "huffman.h"

int main(int argc, char *argv[]) {
	int freq[256] = { 0 };
	int k;
	int entries=-1;

	FILE *inputfile;

	if (argc != 2) {
		printf("Usage: <inputfile>\n");
		exit(1);
	}

	inputfile = fopen(argv[1], "r");

	if (inputfile == NULL) {
		printf("Failed to open");
		exit(1);
	}

	char c;
        unsigned char v;
	while((c=fgetc(inputfile))) {
		if(c == EOF) break;
		v=c;
		freq[v]+=1;
	}
	printf("----------------------Calculating frequencies---------------\n");
	for(k=0; k<256; k++) {
		if(freq[k] > 0) {
			printf("[%c: %d]\n", k, freq[k]);
			entries++;
		}
	}
	printf("--------------------------- Heap --------------------\n");
	for(k=0; k<256; k++) {
		if(freq[k] > 0) {
			printf("[%d: %d]\n", k, freq[k]);
		}
	}

	fclose(inputfile);
	printf("--------------------Encoding for individual characters------- \n");
	return 0;
}
