;*****************************************************************************
;* 1733201: Assemly Project the heap part          *
;*                                        *
;*****************************************************************************

extern malloc
extern free
global heap_initialize
global heap_insert
global heap_remove
;*****************************************************************************
;*void heap_initialize(Heap *H)  
;*****************************************************************************
%define H   [ebp+8]
%define i   [ebp-4]
%define def [ebp-8]
heap_initialize:
  push ebp
  mov  ebp, esp
  mov  ebx, i					
  mov  eax, H
  mov  eax, [eax]
  mov  eax, 0
  mov  H, eax  		
  mov  edx,16
  push edx
  call malloc
  add  esp, 4
  mov  def, eax
  mov  ebx, 0
  jmp  .while				
.while:
  cmp  ebx,  256			
  jnl  .end
  mov  eax, H
  mov  eax, [eax]
  lea  eax, [eax+ebx*4]
  mov  eax, def			
  inc  ebx			
  mov  i, ebx
  jmp  .while
.end:
  mov  esp, ebp
  pop  ebp
  ret

;*****************************************************************************
;*void heap_insert(Heap *H, HeapNode *node)                      
;*****************************************************************************
%define H [ebp+8]
%define node [ebp+12]
%define c    [ebp-4]
%define p    [ebp-8]
%define temp [ebp-12]

heap_insert:
  push  ebp
  mov   ebp, esp
  mov   ecx, 16
  push ecx
  call malloc			
  add  esp, 4
  mov  temp, eax
  mov  edi, node		
  mov  edi, node
  mov  eax, H
  mov  eax, [eax]	
  mov  ecx, H
  imul eax, 16
  lea  edi, [ecx+eax]
  mov  esi, edi
  mov  ecx, 16
  cld
  rep movsd
  mov  c, eax			
  mov  edx, eax 
  dec  edx
  mov  eax, edx
  mov  edx, 0
  mov  ebx, 2
  div  ebx
  mov  p, eax
  jmp  .while
.while:
  mov  eax, p
  cmp  eax, 0
  jnge .end
  mov  edx, p
  mov  ecx, H
  mov  esi, 16
  imul edx, esi
  mov  eax, [ecx+edx]	
  mov  p, eax
  mov  ebx, c
  imul ebx, 16
  mov  ebx, [ecx+ebx]	
  mov  c, ebx
  cmp  eax, ebx			
  jng  .else
  mov  temp, eax		
  mov  eax, ebx			
  mov  ebx, temp		
  mov  ecx, c		
  mov  ecx, p		
  mov  c, ecx		
  mov  eax, c
  mov  edx, 0
  mov  ebx, 2
  div  ebx
  mov  p, eax		
  jmp  .while
.else:
  mov  dword p, dword -1		
  jmp  .while
.end:
  mov  eax, H		
  mov  eax,[eax]
  inc  eax
  mov  H, eax
  mov  eax, temp
  push eax
  call free			
  add  esp, 4
  mov  esp, ebp
  pop  ebp
  ret  

;*****************************************************************************
;*void heap_remove(Heap *H, HeapNode *node);                                
;*****************************************************************************
%define H [ebp + 8]
%define node [ebp + 12]
%define m    [ebp-4]
%define temp [ebp-8]
%define c    [ebp-12]
%define p    [ebp-16]

heap_remove:
 push ebp
 mov  ebp, esp
 mov  ebx , temp		
 moV  eax, 16
 push eax
 call malloc
 add  esp, 4
 mov  temp, eax	
 mov  ecx, H
 mov  ecx, [ecx]
 cmp  ecx, 0		
 jnge .end
 mov  ebx, node
 mov  eax, H
 mov  eax,[eax]
 mov  ecx, H
 mov  edx, 0
 imul edx, 16
 mov  eax, [ecx+edx]
 mov  node, eax		
 mov  edx, H
 mov  edx, [edx]
 dec  edx
 imul edx, 16
 mov  eax,[ecx+edx]		 
 mov  eax, H
 mov  eax, [eax]
 dec  eax		
 mov  dword p, 0		
 mov  ebx, p
 imul ebx, 2
 inc  ebx
 mov  c, ebx		
 jmp  .while
.while:
  mov  edx, c
  mov  edx, [edx]
  inc  edx
  mov  ecx, H
  imul edx, 16
  mov  edx, [ecx+edx]	
  mov  p, edx
  mov  ebx, c
  imul ebx, 16
  mov  ebx, [ecx+ebx]	
  mov  c, ebx
  cmp  ebx, edx			
  jng  .if1
  mov  ebx, c
  inc  ebx
  mov  c, ebx
  jmp  .while
.if1:
  mov  eax, p
  mov  eax, [eax]
  mov  ecx, H
  imul eax, 16
  mov  eax, [ecx+eax]
  mov  p, eax
  mov  ebx, c
  imul ebx, 16
  mov  ebx, [ecx+ebx]	
  mov  c, ebx
  cmp  ebx, eax			
  jng  .else1
  mov  temp, ebx		
  mov  eax, ebx			
  mov  ebx, temp		
  imul ebx, 2
  inc  ebx
  mov  c, ebx		
  mov  ecx, c		
  jmp  .while
.else1:
  mov  ebx, H
  mov  ebx, [eax]
  mov  c, ebx
.end:
mov  esp, ebp
pop  ebp
ret 
