;*****************************************************************************
;* 17332001:Assembler implementation of Huffman encoding                     *
;*        	                                                             *
;*****************************************************************************
extern malloc
extern heap_remove
extern heap_insert

global huffman_build_tree
global huffman_initialize_table
global huffman_build_table
;*****************************************************************************
;* huffman_build_tree(Heap *h, HeapNode **t);                          *
;*****************************************************************************
%define H   [ebp+8]
%define t   [ebp+12]
%define x   [ebp-4] 
%define y   [ebp-8]
%define z   [ebp-12]
huffman_build_tree:
  push 	ebp
  mov 	ebp, esp
  mov 	eax,  H
  mov 	eax, [eax]
  sub	esp, 12
  cmp	eax,  0
.while:
  mov 	eax,  H
  mov 	eax, [eax]
  sub 	esp, 12 
  cmp 	eax,  0
  jle 	.done
  push 	dword 16 
  call 	malloc
  add 	esp, 4
  mov 	 x, eax
  mov	ebx,  x 
  mov 	eax,  H
  mov 	eax, [eax]
  cmp 	eax, 1
  jne 	.else
  mov 	eax,  x
  push 	eax 
  mov	ebx,  H
  push 	ebx
  call 	heap_remove 
  pop 	eax
  add 	esp, 8
  mov 	eax,  t
  mov 	ecx,  x
  mov 	[eax], ecx
  jmp 	.while
.else: 
  mov	eax,  H
  mov	ebx,  x
  push 	ebx
  push 	eax
  call 	heap_remove 
  add 	esp, 8 
  push 	dword 16 
  call 	malloc
  add 	esp, 4
  mov 	 y, eax 
  mov	eax,  H
  mov	ebx,  y
  push 	ebx
  push 	eax				
  call 	heap_remove 
  add 	esp, 8
  push 	dword 16 	
  call 	malloc
  add 	esp, 4
  mov 	 z, eax
  mov 	eax,  x 
  mov 	eax, [eax]
  mov 	edx,  y
  mov 	edx, [edx] 
  mov 	ebx,  z
  add 	edx, eax
  mov 	[ebx], edx
  mov 	eax,  z
  mov 	edi,  y
  mov 	[eax+8], edi	
  mov 	ecx,  x
  mov 	[eax+12], ecx	
  mov 	eax,  z
  push 	eax
  mov	edx,  H
  push 	edx
  call	heap_insert
  pop 	eax
  add 	esp, 8
  jmp 	.while  
.done: 
  mov 	esp, ebp
  pop 	ebp
  ret
;*****************************************************************************
;* void huffman_initialize_table(HuffmanNode *t);                           *
;*****************************************************************************
%define t [ebp+8] 
huffman_initialize_table:
  push	ebp
  mov	ebp, esp
  mov	eax, 0xff
  mov	edi, t
  mov	ecx,  256
  rep	stosb
  mov	esp, ebp
  pop	ebp
  ret
;*****************************************************************************
;* void huffman_build_table(HeapNode *root, HuffmanNode *t, int code, 
;*			     int size);                            
;*****************************************************************************
%define size [ebp+20]
%define code [ebp+16]
%define t    [ebp+12]
%define root [ebp+8]
huffman_build_table:
  push	ebp
  mov 	ebp, esp
  mov 	eax, root
  cmp 	eax, 0
  je 	.done
  mov 	edi, root
  mov 	edx, [edi+8]
  cmp 	edx, 0
  jne 	.l1
  jmp 	.l5
.l1:
  mov 	ebx, 1
  mov 	eax, 31
  sub 	eax, size
  mov 	ecx, eax
.run:
  cmp 	ecx, 0
  je 	.end
  shl 	ebx, 1
  dec 	ecx
  jmp 	.run
.end:
  mov 	eax, code
  add 	ebx, eax
  mov 	eax, ebx
  mov 	ebx, size
  inc 	ebx
  push 	ebx
  push 	eax
  push 	dword t
  mov 	eax, root
  mov 	esi, [eax+8]
  push 	esi
  call 	huffman_build_table
  add 	esp, 16
  jmp 	.l6
.l3:
  mov 	eax, root
  mov 	ebx, [eax+12]
  mov 	ecx, t
  mov 	edx, code
  mov 	eax, size
  inc 	eax
  push 	eax
  push 	edx
  push 	ecx
  push 	ebx
  call 	huffman_build_table
  add 	esp, 16
  jmp 	.done
.l4:
  mov 	eax, root
  mov 	ebx, [eax+12]
  mov 	ecx, t
  mov 	edx, code
  mov 	eax, size
  inc 	eax
  push 	eax
  push 	edx
  push 	ecx
  push 	ebx
  call 	huffman_build_table
  add 	esp, 16
  jmp 	.done
.con:
  mov 	ebx, code
  mov 	ecx, size
  mov 	edi, t
  mov 	esi, root
  mov 	eax, [esi+4]
  imul 	eax, 8
  add 	edi, eax
  mov 	[edi], ebx
  mov 	[edi+4], ecx
  jmp	.done
.l5:
  mov 	eax, root
  mov 	ebx, [eax+12]
  cmp 	ebx, 0
  jne 	.l4
  jmp 	.con
.l6:
  mov 	eax, root
  mov 	ebx, [eax+12]
  cmp 	ebx, 0
  je 	.l4
  jmp 	.l3
.done:
  mov 	esp, ebp
  pop 	ebp
  ret
